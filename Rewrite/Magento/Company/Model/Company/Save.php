<?php

declare(strict_types=1);

namespace Avanti\CompanyAttributes\Rewrite\Magento\Company\Model\Company;

use Magento\Framework\Exception\CouldNotSaveException;

class Save extends \Magento\Company\Model\Company\Save
{

    public function __construct(
        \Magento\Company\Model\SaveHandlerPool $saveHandlerPool,
        \Magento\Company\Model\ResourceModel\Company $companyResource,
        \Magento\Company\Api\Data\CompanyInterfaceFactory $companyFactory,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory,
        \Magento\Company\Model\SaveValidatorPool $saveValidatorPool,
        \Magento\Framework\Stdlib\DateTime\DateTime $date

    ) {
        $this->saveHandlerPool = $saveHandlerPool;
        $this->companyResource = $companyResource;
        $this->companyFactory = $companyFactory;
        $this->saveValidatorPool = $saveValidatorPool;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->date = $date;
    }

    public function save(\Magento\Company\Api\Data\CompanyInterface $company)
    {
        if (is_array($company->getCargoHandling())) {
            $company->setCargoHandling(implode(',', $company->getCargoHandling()));
        }

        if (is_array($company->getCompanyType())) {
            $company->setCompanyType(implode(',', $company->getCompanyType()));
        }

        if (is_array($company->getInterestArea())) {
            $company->setInterestArea(implode(',', $company->getInterestArea()));
        }


        $company['status'] = true;
        $company['reject_reason'] = 'Aprovado automaticamente';
        $company['reject_at'] = $this->date->gmtDate();;


        $this->processAddress($company);
        $this->processSalesRepresentative($company);
        $companyId = $company->getId();
        $initialCompany = $this->getInitialCompany($companyId);
        $this->saveValidatorPool->execute($company, $initialCompany);
        try {
            $this->companyResource->save($company);
            $this->saveHandlerPool->execute($company, $initialCompany);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save company'),
                $e
            );
        }

        return $company;
    }

    /**
     * Get initial company.
     *
     * @param int|null $companyId
     * @return \Magento\Company\Api\Data\CompanyInterface
     */
    private function getInitialCompany($companyId)
    {
        $company = $this->companyFactory->create();
        try {
            $this->companyResource->load($company, $companyId);
        } catch (\Exception $e) {
            //Do nothing, just leave the object blank.
        }

        return $company;
    }

    /**
     * Set default sales representative (admin user responsible for company) if it is not set.
     *
     * @param \Magento\Company\Api\Data\CompanyInterface $company
     * @return void
     */
    private function processSalesRepresentative(\Magento\Company\Api\Data\CompanyInterface $company)
    {
        if (!$company->getSalesRepresentativeId()) {
            /** @var \Magento\User\Model\ResourceModel\User\Collection $userCollection */
            $userCollection = $this->userCollectionFactory->create();
            $company->setSalesRepresentativeId($userCollection->setPageSize(1)->getFirstItem()->getId());
        }
    }

    /**
     * Prepare company address.
     *
     * @param \Magento\Company\Api\Data\CompanyInterface $company
     * @return void
     */
    private function processAddress(\Magento\Company\Api\Data\CompanyInterface $company)
    {
        if (!$company->getRegionId()) {
            $company->setRegionId(null);
        } else {
            $company->setRegion(null);
        }
        $street = $company->getStreet();
        ksort($street);
        if (is_array($street) && count($street)) {
            $company->setStreet(trim(implode("\n", $street)));
        }
    }
}