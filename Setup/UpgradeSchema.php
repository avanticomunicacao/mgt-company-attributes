<?php

namespace Avanti\CompanyAttributes\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.5') < 0) {

            if (!$setup->tableExists('avanti_company_attribute_options')) {
                $table = $setup->getConnection()->newTable(
                    $setup->getTable('avanti_company_attribute_options')
                )
                    ->addColumn(
                        'entity_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'Entity ID'
                    )
                    ->addColumn(
                        'option_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        255,
                        ['nullable => false'],
                        'Option ID'
                    )
                    ->addColumn(
                        'parent_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        255,
                        ['nullable => false'],
                        'Parent ID'
                    )
                    ->addColumn(
                        'option_name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        [],
                        'Option Name'
                    )
                    ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        1,
                        [],
                        'Option Status'
                    )
                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                    )->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                        'Updated At')
                    ->setComment('Company Attribute Options');
                $setup->getConnection()->createTable($table);

                $setup->getConnection()->addIndex(
                    $setup->getTable('avanti_company_attribute_options'),
                    $setup->getIdxName(
                        $setup->getTable('avanti_company_attribute_options'),
                        ['option_name'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['option_name'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }
        
        $setup->endSetup();
    }
}