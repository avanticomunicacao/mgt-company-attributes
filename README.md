# Mage2 Module Avanti CompanyAttributes

    ``avanti/module-companyattributes``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Avanti`
 - Enable the module by running `php bin/magento module:enable Avanti_CompanyAttributes`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require avanti/module-companyattributes`
 - enable the module by running `php bin/magento module:enable Avanti_CompanyAttributes`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - test (test/options/test)


## Specifications




## Attributes

 - Company - cargo_handling (cargo_handling)

 - Company - company_type (company_type)

 - Company - interest_area (interest_area)