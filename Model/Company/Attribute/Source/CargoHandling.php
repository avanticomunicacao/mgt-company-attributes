<?php

namespace Avanti\CompanyAttributes\Model\Company\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Avanti\CompanyAttributes\Model\ResourceModel\Options\CollectionFactory;

class CargoHandling extends AbstractSource
{

    protected $optionsFactory;
    const ATTRIBUTE_ID = 1;

    public function __construct(
        CollectionFactory $optionsFactory 
    )
    {
        $this->optionsFactory = $optionsFactory;
    }

    public function getAllOptions()
    {

        $collection = $this->optionsFactory->create();
        $collection->addFieldToFilter('parent_id', ['eq' => self::ATTRIBUTE_ID]);

        foreach ($collection as $option) {
            if ($option->getOptionId() && $option->getOptionName() && $option->getStatus()) {
                $this->_options[] = [
                    'value' => $option->getOptionId(), 'label' => __($option->getOptionName())
                ];
            }
        }

        return $this->_options;
    }
}