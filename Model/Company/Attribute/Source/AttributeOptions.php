<?php

namespace Avanti\CompanyAttributes\Model\Company\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class AttributeOptions extends AbstractSource
{
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => 0, 'label' => __('Please select an option.')],
                ['value' => 1, 'label' => __('Cargo Handling')],
                ['value' => 2, 'label' => __('Interest Area')],
                ['value' => 3, 'label' => __('Company Type')]
            ];
        }
        
        return $this->_options;
    }
}