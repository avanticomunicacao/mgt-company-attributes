<?php
namespace Avanti\CompanyAttributes\Model\Company;

use Avanti\CompanyAttributes\Model\ResourceModel\Options\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $attributeOptionsCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $attributeOptionsCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();
        foreach ($items as $option) {
            $this->loadedData[$option->getId()]['option'] = $option->getData();
        }


        return $this->loadedData;

    }
}