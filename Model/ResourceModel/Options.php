<?php
namespace Avanti\CompanyAttributes\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Options extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('avanti_company_attribute_options', 'entity_id');
    }
}
