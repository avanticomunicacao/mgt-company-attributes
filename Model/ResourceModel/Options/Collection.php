<?php
namespace Avanti\CompanyAttributes\Model\ResourceModel\Options;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Avanti\CompanyAttributes\Model\Options', 'Avanti\CompanyAttributes\Model\ResourceModel\Options');
    }
}