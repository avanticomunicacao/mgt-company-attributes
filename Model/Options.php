<?php
namespace Avanti\CompanyAttributes\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Options extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'avanti_company_attribute_options';

    protected $_cacheTag = 'avanti_company_attribute_options';
    protected $_eventPrefix = 'avanti_company_attribute_options';

    public function _construct()
    {
        $this->_init('Avanti\CompanyAttributes\Model\ResourceModel\Options');
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        return [];
    }
}