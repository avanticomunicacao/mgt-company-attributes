<?php

namespace Avanti\CompanyAttributes\Block;

use Avanti\CompanyAttributes\Model\Company\Attribute\Source\CompanyType as CompanyTypeSource;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class CompanyType extends Template
{
    protected $companyType;

    public function __construct(
            CompanyTypeSource $companyType,
            Context $context,
            array $data = []
    ) {
        parent::__construct($context, $data);
        $this->companyType = $companyType;
    }

    public function getOptions()
    {
        return $this->companyType->getAllOptions();
    }

    public function issetOptions()
    {
        if (count($this->getOptions()) > 0) {
            return true;
        }

        return false;
    }
}