<?php

namespace Avanti\CompanyAttributes\Block;

use Avanti\CompanyAttributes\Model\Company\Attribute\Source\CargoHandling as CargoHandlingSource;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class CargoHandling extends Template
{
    protected $cargoHandling;

    public function __construct(
            CargoHandlingSource $cargoHandling,
            Context $context,
            array $data = []
    ) {
        parent::__construct($context, $data);
        $this->cargoHandling = $cargoHandling;
    }

    public function getOptions()
    {
        return $this->cargoHandling->getAllOptions();
    }

    public function issetOptions()
    {
        if (count($this->getOptions()) > 0) {
            return true;
        }

        return false;
    }
}