<?php
namespace Avanti\CompanyAttributes\Block\Adminhtml\Company\Edit;

use Magento\Search\Controller\RegistryConstants;
use Magento\Backend\Block\Widget\Context;
use \Magento\Framework\Registry;

class GenericButton
{
    protected $urlBuilder;
    protected $registry;

    public function __construct(
        Context $context,
        Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    public function getId()
    {
        $option = $this->registry->registry('option');
        return $option ? $contact->getId() : null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}