<?php

namespace Avanti\CompanyAttributes\Block;

use Avanti\CompanyAttributes\Model\Company\Attribute\Source\InterestArea as InterestAreaSource;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class InterestArea extends Template
{
    protected $interestArea;

    public function __construct(
            InterestAreaSource $interestArea,
            Context $context,
            array $data = []
    ) {
        parent::__construct($context, $data);
        $this->interestArea = $interestArea;
    }

    public function getOptions()
    {
        return $this->interestArea->getAllOptions();
    }

    public function issetOptions()
    {
        if (count($this->getOptions()) > 0) {
            return true;
        }

        return false;
    }

}