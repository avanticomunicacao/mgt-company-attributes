<?php

declare(strict_types=1);

namespace Avanti\CompanyAttributes\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class System extends AbstractHelper
{
    /**
     * @return mixed
     */
    public function isRewriteEnabled() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::REWRITE_REGISTER_URL,
            ScopeInterface::SCOPE_STORE
        );
    }
}