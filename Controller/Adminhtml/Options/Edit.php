<?php

namespace Avanti\CompanyAttributes\Controller\Adminhtml\Options;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Avanti\CompanyAttributes\Controller\Adminhtml\Options;
use Avanti\CompanyAttributes\Model\OptionsFactory;

class Edit extends Options
{
    private $attributeOptionsFactory;
    private $dataPersistor;
    private $resultPageFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        OptionsFactory $attributeOptionsFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->dataPersistor = $dataPersistor;
        $this->attributeOptionsFactory = $attributeOptionsFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $componentId = $this->getRequest()->getParam('entity_id');
        $optionId = $this->getRequest()->getParam('entity_id');
        if (isset($optionId)) {
            $this->dataPersistor->set('avanti_companyattributes_option_id', $this->getRequest()->getParam('entity_id'));
        }
        $attributeOptionsModel = $this->attributeOptionsFactory->create();
        if ($componentId) {
            $attributeOptionsModel->load($componentId);
            if (!$attributeOptionsModel->getId()) {
                $this->messageManager->addErrorMessage(__('This Option no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('avanti_company_attribute_options', $attributeOptionsModel);

        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $componentId ? __('Edit Attribute Option') : __('New Attribute Option'),
            $componentId ? __('Edit Attribute Option') : __('New Attribute Option')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Attribute Option'));
        $resultPage->getConfig()->getTitle()->prepend($attributeOptionsModel->getId() ? __('Edit Attribute Option %1', $attributeOptionsModel->getId()) : __('New Attribute Option'));
        return $resultPage;
    }
}