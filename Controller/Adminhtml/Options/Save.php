<?php

namespace Avanti\CompanyAttributes\Controller\Adminhtml\Options;

use Avanti\CompanyAttributes\Controller\Adminhtml\Options;
use Avanti\CompanyAttributes\Model\OptionsFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Save extends Options
{
    private $_optionsFactory;

    private $resultPageFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        OptionsFactory $optionsFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_optionsFactory = $optionsFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->isPost()) {
           $input = $this->getRequest()->getPostValue();
           $option = $this->_optionsFactory->create();

            if (isset($input['option']['entity_id'])) {
                    $option->load($input['option']['entity_id']);
                    $option->addData($input['option']);
                    $option->setId($input['option']['entity_id']);
                    $option->save();
            } else {
               $option->setData($input['option'])->save();
            }

            return $resultRedirect->setPath('*/*/');
        }
    }
}