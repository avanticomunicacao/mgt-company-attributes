<?php

namespace Avanti\CompanyAttributes\Controller\Adminhtml\Options;

use Avanti\CompanyAttributes\Controller\Adminhtml\Options;
use Avanti\CompanyAttributes\Model\OptionsFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Options
{
    private $attributeOptionsFactory;

    private $resultPageFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        OptionsFactory $attributeOptionsFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->attributeOptionsFactory = $attributeOptionsFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $optionId = $this->getRequest()->getParam('entity_id');
        if ($optionId) {
            try {
                $attributeOptionsModel = $this->attributeOptionsFactory->create();
                $attributeOptionsModel->load($optionId);
                $attributeOptionsModel->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the Option.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $optionId]);
            }
        }
        $this->messageManager->addErrorMessage(__("We can't find a Option to delete."));
        return $resultRedirect->setPath('*/*/');
    }
}