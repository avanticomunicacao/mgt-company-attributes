<?php

namespace Avanti\CompanyAttributes\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\App\Action;

abstract class Options extends Action
{
    const ADMIN_RESOURCE = 'Avanti_CompanyAttributes::main';

    protected $_coreRegistry;

    public function __construct(Context $context, Registry $coreRegistry)
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Avanti'), __('Avanti'))
            ->addBreadcrumb(__('Attribute Options'), __('Attribute Options'));

        return $resultPage;
    }
}