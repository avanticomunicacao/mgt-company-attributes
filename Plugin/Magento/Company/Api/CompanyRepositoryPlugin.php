<?php


namespace Avanti\CompanyAttributes\Plugin\Magento\Company\Api;

use Magento\Company\Model\CompanyRepository;
use Magento\Company\Api\CompanyRepositoryInterface;

class CompanyRepositoryPlugin
{

    protected $companyRepository;
    protected $companyExtensionFactory;

    public function __construct(
        CompanyRepository $companyRepository,
        \Magento\Company\Api\Data\CompanyExtensionFactory $companyExtensionFactory
    ) {
        $this->companyRepository = $companyRepository;
        $this->companyExtensionFactory = $companyExtensionFactory;
    }

    public function afterGet(
        CompanyRepositoryInterface $subject,
        $result
    ) {
        $company = $result;
        $extensionAttributes = $company->getExtensionAttributes();
        $companyExtension = $extensionAttributes ? $extensionAttributes : $this->companyExtensionFactory->create();
            
        $companyExtension->setCompanyType($company->getData('company_type'));
        $companyExtension->setInterestArea($company->getData('interest_area'));
        $companyExtension->setCargoHandling($company->getData('cargo_handling'));

        $company->setExtensionAttributes($companyExtension);
        return $company;
    }

    public function afterSave(
        CompanyRepositoryInterface $subject,
        $result
    ) {
        $company = $result;
        $extensionAttributes = $company->getExtensionAttributes();
        if (!$extensionAttributes) {
            return $company;
        }
        
        $company->setData('company_type', $extensionAttributes->getCompanyType());
        $company->setData('interest_area', $extensionAttributes->getInterestArea());
        $company->setData('cargo_handling', $extensionAttributes->getCargoHandling());

        $company->save();
        return $company;
    }

    public function afterGetList(
        CompanyRepositoryInterface $subject,
        $result
    ) {
        foreach ($result->getItems() as $company) {
            $this->afterGet($subject, $company);
        }
        return $result;
    }
}