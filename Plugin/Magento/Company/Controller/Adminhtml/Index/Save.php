<?php declare(strict_types=1);


namespace Avanti\CompanyAttributes\Plugin\Magento\Company\Controller\Adminhtml\Index;


class Save
{

    /**
     * @param \Magento\Company\Controller\Adminhtml\Index\Save $subject
     * @param $result
     * @return mixed
     */
    public function afterSetCompanyRequestData(
        \Magento\Company\Controller\Adminhtml\Index\Save $subject,
        $result
    ) {
        $result->setData('company_type', implode(',', $subject->getRequest()->getPostValue('general')['company_type']));
        $result->setData('interest_area', implode(',', $subject->getRequest()->getPostValue('general')['interest_area']));
        $result->setData('cargo_handling', implode(',', $subject->getRequest()->getPostValue('general')['cargo_handling']));

        return $result;
    }
}