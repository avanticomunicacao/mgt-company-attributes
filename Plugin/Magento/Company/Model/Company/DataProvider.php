<?php declare(strict_types=1);


namespace Avanti\CompanyAttributes\Plugin\Magento\Company\Model\Company;


class DataProvider
{

    /**
     * @param \Magento\Company\Model\Company\DataProvider $subject
     * @param $result
     * @return mixed
     */
    public function afterGetGeneralData(
        \Magento\Company\Model\Company\DataProvider $subject,
        $result,
        \Magento\Company\Api\Data\CompanyInterface $company
    ) {

        $result['cargo_handling'] = $company->getData('cargo_handling');

        $result['company_type'] = $company->getData('company_type');

        $result['interest_area'] = $company->getData('interest_area');

        return $result;
    }
}

